# template.sh - edit this file
function usage
{
  echo "usage: $0 arguments ..."
  if [ $# -eq 1 ]
  then echo "ERROR: $1"
  fi
}

# Your script starts after this line.

if [ $# -gt 0 ]
	then
	echo "Faustino Perez"
	date '+%a %b %d %T PDT %Y'
	echo ""	
		
	while [ $# -gt 0 ] 
	do
			
		case $1 in
			TestError)
				usage "$1 found"
				echo "*****"
				;;
			now)
				date '+It is now %r'
				echo "*****"
				;;	
			*)			
				usage "Do not know that to do with $1"
				echo "*****"
				;;
		esac
		shift
	done	
else
	usage 			
 
fi


